report 50100 "SwS 365 G/L Journal Import"
{
    Caption = 'SwS 365 G/L Journal Import';
    UsageCategory = Tasks;
    ProcessingOnly = true;

    dataset
    {
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field("Journal Template Name"; ReqTemplate)
                    {
                        ApplicationArea = All;
                        Caption = 'Journal Template Name';
                        TableRelation = "Gen. Journal Template";
                    }
                    field("Journal Batch Name"; ReqBatch)
                    {
                        ApplicationArea = All;
                        Caption = 'Journal Batch Name';

                        trigger OnLookup(var Text: Text): Boolean
                        var
                            GenJournalLine: Record "Gen. Journal Line";
                            CurrentJnlBatchName: Code[10];
                        begin
                            with GenJournalLine do begin

                                FilterGroup := 2;
                                SetRange("Journal Template Name", ReqTemplate);
                                SetRange("Journal Batch Name", ReqBatch);
                                FilterGroup := 0;

                                GenJnlManagement.LookupName(CurrentJnlBatchName, GenJournalLine);

                                FilterGroup := 2;
                                ReqBatch := CopyStr(GetFilter("Journal Batch Name"), 1, 10);
                                FilterGroup := 0;

                            end;
                        end;

                        trigger OnValidate()
                        var
                            GenJournalBatch: Record "Gen. Journal Batch";
                        begin
                            with GenJournalBatch do begin

                                SetRange("Journal Template Name", ReqTemplate);
                                SetFilter(Name, '%1', '@' + ReqBatch + '*');
                                FindFirst();
                                ReqBatch := Name;

                            end;
                        end;
                    }
                    field("File Name"; ReqFile)
                    {
                        ApplicationArea = All;
                        AssistEdit = true;
                        Caption = 'File Name';
                        Editable = false;

                        trigger OnAssistEdit()
                        begin
                            ReqFile := '';
                            UploadIntoStream(RequestOptionsPage.Caption(), '', '(*.txt)|*.txt', ReqFile, UploadFileInStream);
                        end;
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnOpenPage()
        begin
            ReqFile := SelectFileTxt;
        end;
    }

    labels
    {
    }

    trigger OnPreReport()
    begin
        StartImport();
    end;

    var
        JournalLineDimension: Record "Dimension Set Entry" temporary;
        GenJnlManagement: Codeunit GenJnlManagement;
        ReqFile: Text;
        ReqTemplate: Code[10];
        ReqBatch: Code[10];
        LineNumber: Integer;
        UploadFileInStream: InStream;
        ReadLineMsg: Label 'Read line #1#######';
        SelectFileTxt: Label 'Please select a file:   --->';
        SpecifyEmptyJournalErr: Label 'Please specify an empty journal.';
        TotalLinesReadMsg: Label 'Total %1 lines read.';

    local procedure StartImport()
    var
        GLAccount: Record "G/L Account";
        GenJournalLine: Record "Gen. Journal Line";
        FieldInput: array[200] of Text;
        FieldNumber: Integer;
        Counter: Integer;
        UserDialog: Dialog;
        InputString: Char;
        ImportAmount: Decimal;
        ImportDate: Date;
    begin
        FieldNumber := 1;

        UserDialog.Open(ReadLineMsg);

        GenJournalLine.SetRange("Journal Template Name", ReqTemplate);
        GenJournalLine.SetRange("Journal Batch Name", ReqBatch);
        if GenJournalLine.FindFirst() then
            Error(SpecifyEmptyJournalErr);

        while UploadFileInStream.Read(InputString) <> 0 do begin

            if InputString > 31 then
                FieldInput[FieldNumber] := FieldInput[FieldNumber] + Format(InputString);

            if InputString = 59 then begin

                if StrLen(FieldInput[FieldNumber]) > 0 then
                    FieldInput[FieldNumber] := CopyStr(FieldInput[FieldNumber], 1, StrLen(FieldInput[FieldNumber]) - 1);
                FieldInput[FieldNumber] := DelChr(FieldInput[FieldNumber], '>', ' ');

                FieldNumber += 1;
            end;

            if InputString = 10 then begin  // When Linefeed do Transfer
                Counter := Counter + 1;
                UserDialog.Update(1, Counter);

                Evaluate(ImportAmount, FieldInput[7]);

                if ImportAmount <> 0 then begin
                    GLAccount.Get(FieldInput[3]);
                    GLAccount.TestField(Blocked, false);
                    GLAccount.TestField("Account Type", 0);

                    with GenJournalLine do begin
                        Init();
                        Validate("Journal Template Name", ReqTemplate);
                        Validate("Journal Batch Name", ReqBatch);

                        LineNumber += 10000;

                        "Line No." := LineNumber;
                        Insert(true);

                        Evaluate(ImportDate, FieldInput[1]);
                        Validate("Posting Date", ImportDate);
                        Validate("Document Date", ImportDate);
                        Validate("Document No.", FieldInput[2]);
                        Validate("Account Type", "Account Type"::"G/L Account");
                        "System-Created Entry" := true;
                        Validate("Account No.", FieldInput[3]);
                        Validate(Description, FieldInput[4]);
                        Validate("Shortcut Dimension 1 Code", FieldInput[9]);
                        Validate("Shortcut Dimension 2 Code", FieldInput[11]);
                        "Source Code" := FieldInput[6];
                        if FieldInput[5] <> '' then
                            Validate("VAT Prod. Posting Group", FieldInput[5]);
                        Validate(Amount, ImportAmount);
                        Modify(true);

                        JournalLineDimension.Reset();
                        JournalLineDimension.DeleteAll();

                        WriteOtherDimension(FieldInput[8], FieldInput[9]);
                        WriteOtherDimension(FieldInput[10], FieldInput[11]);

                        WriteOtherDimension(FieldInput[12], FieldInput[13]);
                        WriteOtherDimension(FieldInput[14], FieldInput[15]);
                        WriteOtherDimension(FieldInput[16], FieldInput[17]);
                        WriteOtherDimension(FieldInput[18], FieldInput[19]);
                        WriteOtherDimension(FieldInput[20], FieldInput[21]);
                        WriteOtherDimension(FieldInput[22], FieldInput[23]);
                        WriteOtherDimension(FieldInput[24], FieldInput[25]);
                        WriteOtherDimension(FieldInput[26], FieldInput[27]);

                        "Dimension Set ID" := JournalLineDimension.GetDimensionSetID(JournalLineDimension);

                        Modify(true);

                    end;

                end;

                Clear(FieldInput);
                FieldNumber := 1;
            end;

        end;

        UserDialog.Close();

        Message(TotalLinesReadMsg, Counter);
    end;

    local procedure WriteOtherDimension(pDimension: Code[20]; pDimensionValue: Code[20])
    begin
        if pDimensionValue = '' then
            exit;

        with JournalLineDimension do begin

            Init();
            "Dimension Code" := pDimension;
            "Dimension Value Code" := pDimensionValue;
            if not Insert(true) then
                Modify(true);

        end;
    end;
}