# SwissSalary 365 G/L Connector

"SwS 365 G/L Connector" is an example [Microsoft Business Central per tenant extension](https://docs.microsoft.com/en-us/dynamics365/business-central/ui-extensions),
which provides the import of general ledger entries, created by the [SwissSalary 365 App](https://appsource.microsoft.com/en-us/product/dynamics-365-business-central/PUBID.swisssalary%7CAID.swisssalary%7CPAPPID.ca3d5715-ac87-48ff-ace2-fc1605e50a69?tab=Overview), into another instance of Business Central.


## Usage

### On the source Business Central instance (with the installed SwissSalary 365 App)
- Open the _Payroll Setup_ and check that in the _Setup_ section the field _Interface_ is set to _Dynamics NAV_
- After posting the payroll, a download window appears with the request to save the interface file. Save it in a place where you can find it again.

### On the target Business Central instance
- Find the _SwS 365 G/L Journal Import_ report trough _Tell me_ (the magnifier icon on the top right) and run it
- Select the previous downloaded file
- Choose an _Journal Template_ and an empty _Journal Batch_
- Run the import report


## Contributing
Pull requests are welcome. 

For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)